<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $emailuser = session('emailuser');

        $data = DB::table('image')->where('uuid',session('uuid'))->get();

        if(isset($_POST['mime'])){
            if($_POST['mime'] != ""){
                if($_POST['mime'] == "jpg"){
                    $data = DB::table('image')
                        ->where('uuid',session('uuid'))
                        ->where('mime','image/jpg')
                        ->orwhere('mime','image/jpeg')
                        ->get();
                }else{
                    $data = DB::table('image')
                    ->where('uuid',session('uuid'))
                    ->where('mime',$_POST['mime'])
                    ->get();
                }
            }
        }

        return view('dashboard',['emailuser' => $emailuser,'photos'=> $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
