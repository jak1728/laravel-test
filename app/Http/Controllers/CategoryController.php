<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $emailuser = session('emailuser');

        $data = DB::table('category')
                   ->select('*', DB::raw(' (select count(*) as nums from category_image where category_image.cate_id = category.id) as images'))
                   ->where('uuid', session('uuid'))
                   ->get();

        return view('category',['emailuser' => $emailuser,'category' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $emailuser = session('emailuser');

        $data = DB::table('image')
            ->where('uuid',session('uuid'))
            ->whereRaw('image.id not in (select image_id from category_image)')
            ->get();

        return view("cate-create",['emailuser' => $emailuser,'photos' => $data,'selected' => array()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'cate_name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{

            DB::table('category')->insert([
                'uuid' => session('uuid'),
                'name' => $request->post('cate_name'),
                'created_at' => date('y-m-d H:i:s')
            ]);

            $cate_id = DB::getPdo()->lastInsertId();

            if(null !== $request->post('list')){
                foreach($request->post('list') as $id){
                    DB::table('category_image')->insert([
                        'cate_id' => $cate_id,
                        'image_id' => $id,
                        'created_at' => date('y-m-d H:i:s')
                    ]);
                };
            }

            return redirect('/category');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $emailuser = session('emailuser');

        $data = DB::table('category')
                    ->where('uuid',session('uuid'))
                    ->where('id',$id)
                    ->get();            

        $images = DB::table('category')
                    ->join('category_image', 'category.id', '=', 'category_image.cate_id')
                    ->join('image', 'category_image.image_id', '=', 'image.id')
                    ->where('category.id',$id)
                    ->get();

        $summary = DB::table('category')
                    ->select(DB::raw('SUM(size) as summary'))
                    ->join('category_image', 'category.id', '=', 'category_image.cate_id')
                    ->join('image', 'category_image.image_id', '=', 'image.id')
                    ->where('category.id',$id)
                    ->get();

        if(isset($_POST['mime'])){
            if($_POST['mime'] != ""){
                if($_POST['mime'] == "jpg"){
                    $images = DB::table('category')
                        ->join('category_image', 'category.id', '=', 'category_image.cate_id')
                        ->join('image', 'category_image.image_id', '=', 'image.id')
                        ->where('category.id',$id)
                        ->where(function ($query) {
                            $query
                                ->where('image.mime','image/jpg')
                                ->orwhere('image.mime','image/jpeg');
                        })
                        ->get();

                    $summary = DB::table('category')
                        ->select(DB::raw('SUM(size) as summary'))
                        ->join('category_image', 'category.id', '=', 'category_image.cate_id')
                        ->join('image', 'category_image.image_id', '=', 'image.id')
                        ->where('category.id',$id)
                        ->where(function ($query) {
                            $query
                                ->where('image.mime','image/jpg')
                                ->orwhere('image.mime','image/jpeg');
                        })
                        ->get();    
                }else{
                    $images = DB::table('category')
                        ->join('category_image', 'category.id', '=', 'category_image.cate_id')
                        ->join('image', 'category_image.image_id', '=', 'image.id')
                        ->where('category.id',$id)
                        ->where('image.mime',$_POST['mime'])
                        ->get();

                    $summary = DB::table('category')
                        ->select(DB::raw('SUM(size) as summary'))
                        ->join('category_image', 'category.id', '=', 'category_image.cate_id')
                        ->join('image', 'category_image.image_id', '=', 'image.id')
                        ->where('category.id',$id)
                        ->where('image.mime',$_POST['mime'])
                        ->get();     
                }
            }
        }
        
        $a = json_decode($summary,true);

        return view("viewimages",['emailuser' => $emailuser,'cate' => $data[0],'photos' => $images,'summary'=>$a[0]['summary']]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $emailuser = session('emailuser');

        $cate = DB::table('category')
            ->where('uuid',session('uuid'))
            ->where('id',$id)
            ->get();

        $data = DB::table('image')
            ->where('uuid',session('uuid'))
            ->whereRaw('image.id not in (select image_id from category_image where cate_id != "'.$id.'")')
            ->get();

        $selected = DB::table('category_image')
            ->where("cate_id",$id)
            ->get();

        $selectedNew = array();
        foreach($selected as $sel){
            array_push($selectedNew,$sel->image_id);
        }

        return view("cate-create",['emailuser' => $emailuser,'cate' => $cate[0],'photos' => $data,'selected' => $selectedNew]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'cate_name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{

            DB::table('category')
              ->where('id', $id)
              ->update([
                'name' => $request->post('cate_name'),
                'updated_at' => date('y-m-d H:i:s')
            ]);

            DB::table('category_image')
                ->where('cate_id',$id)
                ->delete();

            $cate_id = $id;

            if(null !== $request->post('list')){
                foreach($request->post('list') as $id){
                    DB::table('category_image')->insert([
                        'cate_id' => $cate_id,
                        'image_id' => $id,
                        'created_at' => date('y-m-d H:i:s')
                    ]);
                };
            }

            return redirect('/category');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('category_image')->where('cate_id',$id)->delete();
        DB::table('category')->where('id',$id)->delete();

        return redirect('/category');
    }
}
