<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dologin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ]);

        if ($validator->fails()) {
            return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $userdata = array(
                'email' => $request->email,
                'password' => $request->password
            );

            if (Auth::attempt($userdata)) {
                // Authentication passed...

                //echo $request->session()->get('key');
                session(['emailuser' => $request->email]);
                session(['uuid' => Auth::id()]);
                session(['role' => 'user']);

                return redirect()->intended('dashboard');
            }else{
                return redirect('/')->withErrors("password not correct!!")
                ->withInput();
            }
        }
    }
}
