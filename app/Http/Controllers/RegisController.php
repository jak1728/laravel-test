<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RegisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3',
            'con_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {

            return back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{
            $userdata = array(
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'created_at' => date('Y-m-d H:i:s')
            );

            DB::table('users')->insert([
                'name'     => $userdata['name'],
                'email'    => $userdata['email'],
                'password' => Hash::make($userdata['password']),
                'created_at' => date('y-m-d H:i:s')
            ]);

            return redirect('/');
        }
    }
}
