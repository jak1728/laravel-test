<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $emailuser = session('emailuser');
        return view('upload',['emailuser' => $emailuser]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'image_name' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }else{

            $fileimage = Storage::putFile("public/images", $request->file('file'));
            
            DB::table('image')->insert([
                'uuid' => session('uuid'),
                'name' => $request->post('image_name'),
                'link' => $fileimage,
                'size' => $request->file('file')->getSize(),
                'mime' => $request->file('file')->getMimeType(),
                'created_at' => date('y-m-d H:i:s')
            ]);

            return redirect('/dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $emailuser = session('emailuser');

        $data = DB::table('image')->where('id',$id)->get();

        return view('upload',['emailuser' => $emailuser,'photo' => $data[0]]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if($request->file('file') !== null){
            $fileimage = Storage::putFile("public/images", $request->file('file'));

            DB::table('image')
                  ->where('id', $id)
                  ->update([
                    'name' => $request->post('image_name'),
                    'link' => $fileimage,
                    'size' => $request->file('file')->getSize(),
                    'mime' => $request->file('file')->getMimeType(),
                    'updated_at' => date('y-m-d H:i:s')
                  ]);
        }else{
            DB::table('image')
                  ->where('id', $id)
                  ->update([
                    'name' => $request->post('image_name'),
                    'updated_at' => date('y-m-d H:i:s')
                  ]);
        }

        return redirect('/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('image')->where('id',$id)->delete();
        DB::table('category_image')->where('image_id',$id)->delete();

        return redirect('/dashboard');

    }
}
