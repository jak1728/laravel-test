<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login', [LoginController::class, 'dologin'])->name('login');
Route::get('/register', [RegisController::class, 'index'])->name('register');
Route::post('/register', [RegisController::class, 'create'])->name('register');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::post('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::post('/upload/{id}', [UploadController::class, 'update'])->name('upload.edit');
Route::post('/upload', [UploadController::class, 'create'])->name('upload.create');
Route::get('/upload/{id}/edit', [UploadController::class, 'edit'])->name('upload');
Route::get('/upload/delete/{id}', [UploadController::class, 'destroy'])->name('upload');
Route::get('/upload', [UploadController::class, 'index'])->name('upload');

Route::post('/category/{id}/view', [CategoryController::class, 'show'])->name('category');
Route::post('/category/{id}', [CategoryController::class, 'update'])->name('category.edit');
Route::post('/category', [CategoryController::class, 'store'])->name('category.create');

Route::get('/category/{id}/view', [CategoryController::class, 'show'])->name('category');
Route::get('/category/{id}/edit', [CategoryController::class, 'edit'])->name('category');
Route::get('/category/delete/{id}', [CategoryController::class, 'destroy'])->name('category');
Route::get('/category/create', [CategoryController::class, 'create'])->name('category');
Route::get('/category', [CategoryController::class, 'index'])->name('category');
//Route::resource('/category', CategoryController::class);


