<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'name'     => 'Prajak',
            'username' => 'columbuzz',
            'email'    => 'buzz2512@abc.io',
            'password' => Hash::make('12345'),
            'created_at' => date('y-m-d H:i:s')
        ]);
    }

}
